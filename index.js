const horses = [
  new Horse('Arkle'),
  new Horse('Red Rum'),
  new Horse('Frankle'),
  new Horse('Charisma'),
];

function Horse(name) {
  this.name = name;
  this.run = function () {
    return new Promise(resolve => {
      let horseTime = Math.floor(500 + Math.random() * (3000 + 1 - 500));
      //if (this.name === 'Arkle') horseTime = 500;
      setTimeout(() => {
        resolve({
          name: name,
          time: horseTime
        });
      }, horseTime)
    })
  }
}

function showHorses() {
  horses.forEach(element => {
    console.log(element.name);
  });
}
function showAccount() {
  console.log(player.account);
}
function setWager(name, bet) {
  if (player.account - bet < 0) {
    throw new Error("У вас недостаточно денег");
  }
  else if (bet < 0) {
    throw new Error("Ставка должна быть > 0");
  }
  player.account -= bet;
  player.wagersArray.push({ horse: name, bet: bet });
}

function startRacing() {
  const racePromises = [];
  horses.forEach(element => {
    racePromises.push(element.run())
  });

  let winner;
  Promise.race(racePromises)
    .then(promise => {
      winner = promise.name;
    }).catch(() => {
      console.error('Error on Promise.race');
    });
  Promise.all(racePromises)
    .then(promises => {
      promises.forEach(element => {
        console.log(`${element.name} - ${element.time}`);
      });
      checkWins();
    })
    .catch(() => {
      console.error('Error on Promise.all');
    });

  function checkWins() {
    let gain = 0;
    for (let element of player.wagersArray) {
      if (element.horse === winner) {
        gain += element.bet * 2;
        player.account += element.bet * 2;
      }
    }
    console.log(`Вы выиграли ${gain}, текущий счет ${player.account}`);
    player.wagersArray = [];
  }
}

let player;
newGame();

function Player(account) {
  this.account = account;
  this.wagersArray = [];
}

function newGame(account = 100) {
  player = new Player(account);
}
